﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adpersonalinfo.aspx.cs" Inherits="ArmansHRM.adpersonalinfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Personal Information Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                  <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="SerialNumber" DataSourceID="hrmdbSqlDataSource" CssClass="table table-bordered table-striped table-hover table-responsive">
                                <Columns>
                                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                                    <asp:TemplateField HeaderText="SerialNumber" InsertVisible="False" SortExpression="SerialNumber">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Username" SortExpression="Username">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" ReadOnly="true" runat="server" Text='<%# Bind("Username") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Username") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type" SortExpression="Type">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox2" ReadOnly="true" runat="server" Text='<%# Bind("Type") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FirstName" SortExpression="FirstName">
                                        <EditItemTemplate>
                                            <asp:RequiredFieldValidator ID="FullNameRequiredValidator" runat="server" ErrorMessage="Every person has a name..." ControlToValidate="TextBox3" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="FullNameRegexValidator" ControlToValidate="TextBox3" runat="server" ErrorMessage="Numerical name? :O" ValidationExpression="^([a-zA-Z]+(_[a-zA-Z]+)*)(\s([a-zA-Z]+(_[a-zA-Z]+)*))*$" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DateOfBirth" SortExpression="DateOfBirth">
                                        <EditItemTemplate>
                                            <asp:RequiredFieldValidator ID="DOBRequiredValidator" runat="server" ErrorMessage="Enter Birthday Please!" ControlToValidate="TextBox4" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("DateOfBirth") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("DateOfBirth") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Gender" SortExpression="Gender">
                                        <EditItemTemplate>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="TextBox6" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("Gender") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("Gender") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Department" SortExpression="Department">
                                        <EditItemTemplate>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="TextBox5" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Department") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EmployeeStatus" SortExpression="EmployeeStatus">
                                        <EditItemTemplate>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="TextBox7" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("EmployeeStatus") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label8" runat="server" Text='<%# Bind("EmployeeStatus") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Paygrade" SortExpression="Paygrade">
                                        <EditItemTemplate>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" ControlToValidate="TextBox8" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("Paygrade") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label9" runat="server" Text='<%# Bind("Paygrade") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Supervisor" SortExpression="Supervisor">
                                        <EditItemTemplate>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" ControlToValidate="TextBox9" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("Supervisor") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label10" runat="server" Text='<%# Bind("Supervisor") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qualification" SortExpression="Qualification">
                                        <EditItemTemplate>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required" ControlToValidate="TextBox10" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("Qualification") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label11" runat="server" Text='<%# Bind("Qualification") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            

                            <asp:SqlDataSource ID="hrmdbSqlDataSource" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [personaldetails] WHERE [SerialNumber] = @original_SerialNumber AND (([Username] = @original_Username) OR ([Username] IS NULL AND @original_Username IS NULL)) AND (([Type] = @original_Type) OR ([Type] IS NULL AND @original_Type IS NULL)) AND (([FirstName] = @original_FirstName) OR ([FirstName] IS NULL AND @original_FirstName IS NULL)) AND (([DateOfBirth] = @original_DateOfBirth) OR ([DateOfBirth] IS NULL AND @original_DateOfBirth IS NULL)) AND (([Gender] = @original_Gender) OR ([Gender] IS NULL AND @original_Gender IS NULL)) AND (([Department] = @original_Department) OR ([Department] IS NULL AND @original_Department IS NULL)) AND (([EmployeeStatus] = @original_EmployeeStatus) OR ([EmployeeStatus] IS NULL AND @original_EmployeeStatus IS NULL)) AND (([Paygrade] = @original_Paygrade) OR ([Paygrade] IS NULL AND @original_Paygrade IS NULL)) AND (([Supervisor] = @original_Supervisor) OR ([Supervisor] IS NULL AND @original_Supervisor IS NULL)) AND (([Qualification] = @original_Qualification) OR ([Qualification] IS NULL AND @original_Qualification IS NULL))" InsertCommand="INSERT INTO [personaldetails] ([Username], [Type], [FirstName], [DateOfBirth], [Gender], [Department], [EmployeeStatus], [Paygrade], [Supervisor], [Qualification]) VALUES (@Username, @Type, @FirstName, @DateOfBirth, @Gender, @Department, @EmployeeStatus, @Paygrade, @Supervisor, @Qualification)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [personaldetails]" UpdateCommand="UPDATE [personaldetails] SET [Username] = @Username, [Type] = @Type, [FirstName] = @FirstName, [DateOfBirth] = @DateOfBirth, [Gender] = @Gender, [Department] = @Department, [EmployeeStatus] = @EmployeeStatus, [Paygrade] = @Paygrade, [Supervisor] = @Supervisor, [Qualification] = @Qualification WHERE [SerialNumber] = @original_SerialNumber AND (([Username] = @original_Username) OR ([Username] IS NULL AND @original_Username IS NULL)) AND (([Type] = @original_Type) OR ([Type] IS NULL AND @original_Type IS NULL)) AND (([FirstName] = @original_FirstName) OR ([FirstName] IS NULL AND @original_FirstName IS NULL)) AND (([DateOfBirth] = @original_DateOfBirth) OR ([DateOfBirth] IS NULL AND @original_DateOfBirth IS NULL)) AND (([Gender] = @original_Gender) OR ([Gender] IS NULL AND @original_Gender IS NULL)) AND (([Department] = @original_Department) OR ([Department] IS NULL AND @original_Department IS NULL)) AND (([EmployeeStatus] = @original_EmployeeStatus) OR ([EmployeeStatus] IS NULL AND @original_EmployeeStatus IS NULL)) AND (([Paygrade] = @original_Paygrade) OR ([Paygrade] IS NULL AND @original_Paygrade IS NULL)) AND (([Supervisor] = @original_Supervisor) OR ([Supervisor] IS NULL AND @original_Supervisor IS NULL)) AND (([Qualification] = @original_Qualification) OR ([Qualification] IS NULL AND @original_Qualification IS NULL))">
                                
                                <DeleteParameters>
                                    <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                                    <asp:Parameter Name="original_Username" Type="String" />
                                    <asp:Parameter Name="original_Type" Type="String" />
                                    <asp:Parameter Name="original_FirstName" Type="String" />
                                    <asp:Parameter Name="original_DateOfBirth" Type="String" />
                                    <asp:Parameter Name="original_Gender" Type="String" />
                                    <asp:Parameter Name="original_Department" Type="String" />
                                    <asp:Parameter Name="original_EmployeeStatus" Type="String" />
                                    <asp:Parameter Name="original_Paygrade" Type="String" />
                                    <asp:Parameter Name="original_Supervisor" Type="String" />
                                    <asp:Parameter Name="original_Qualification" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="Username" Type="String" />
                                    <asp:Parameter Name="Type" Type="String" />
                                    <asp:Parameter Name="FirstName" Type="String" />
                                    <asp:Parameter Name="DateOfBirth" Type="String" />
                                    <asp:Parameter Name="Gender" Type="String" />
                                    <asp:Parameter Name="Department" Type="String" />
                                    <asp:Parameter Name="EmployeeStatus" Type="String" />
                                    <asp:Parameter Name="Paygrade" Type="String" />
                                    <asp:Parameter Name="Supervisor" Type="String" />
                                    <asp:Parameter Name="Qualification" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="Username" Type="String" />
                                    <asp:Parameter Name="Type" Type="String" />
                                    <asp:Parameter Name="FirstName" Type="String" />
                                    <asp:Parameter Name="DateOfBirth" Type="String" />
                                    <asp:Parameter Name="Gender" Type="String" />
                                    <asp:Parameter Name="Department" Type="String" />
                                    <asp:Parameter Name="EmployeeStatus" Type="String" />
                                    <asp:Parameter Name="Paygrade" Type="String" />
                                    <asp:Parameter Name="Supervisor" Type="String" />
                                    <asp:Parameter Name="Qualification" Type="String" />
                                    <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                                    <asp:Parameter Name="original_Username" Type="String" />
                                    <asp:Parameter Name="original_Type" Type="String" />
                                    <asp:Parameter Name="original_FirstName" Type="String" />
                                    <asp:Parameter Name="original_DateOfBirth" Type="String" />
                                    <asp:Parameter Name="original_Gender" Type="String" />
                                    <asp:Parameter Name="original_Department" Type="String" />
                                    <asp:Parameter Name="original_EmployeeStatus" Type="String" />
                                    <asp:Parameter Name="original_Paygrade" Type="String" />
                                    <asp:Parameter Name="original_Supervisor" Type="String" />
                                    <asp:Parameter Name="original_Qualification" Type="String" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                            
                            <br /><br /><br />
                            <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Add New Value" CssClass="btn btn-danger" OnClick="Button1_Click" />

                        </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
                  
</asp:Content>
