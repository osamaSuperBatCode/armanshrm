﻿<%@ Page Title="" Language="C#" MasterPageFile="~/employeemaster.Master" AutoEventWireup="true" CodeBehind="emworkshifts_add.aspx.cs" Inherits="ArmansHRM.emworkshifts_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Workshift Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            <asp:Label ID="lblEmpId" runat="server" Text="Employee Name: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlEmpId" CssClass="form-control m-bot15" runat="server" DataSourceID="SqlDataSource1" DataTextField="FirstName" DataValueField="FirstName">
                                
                            </asp:DropDownList>
                                
                            </div><br /><br /><br />
                          
                            <asp:RequiredFieldValidator ID="WorkshiftTypeRequiredValidator" runat="server" ErrorMessage="Required Workshift Type!" ControlToValidate="txtWorkshiftType" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblWorkshiftType" runat="server" Text="Workshift Type Name: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtWorkshiftType" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required Duration!" ControlToValidate="txtDuration" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="DurationRegexValidator" ControlToValidate="txtDuration" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Only between 1-10" Type="Integer" MaximumValue="10" MinimumValue="1" ControlToValidate="txtDuration" ForeColor="#C66C42"></asp:RangeValidator>
                            <asp:Label ID="lblDuration" runat="server" Text="Duration: " CssClass="control-label col-sm-2"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtDuration" runat="server" CssClass="form-control"></asp:TextBox>
                            </div><br /><br /><br />

                            <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-info" OnClick="Button1_Click" />
                            <asp:Label ID="Label1" runat="server" Text="" CssClass="alert-success"></asp:Label>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [FirstName] FROM [personaldetails]"></asp:SqlDataSource>

                         </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
