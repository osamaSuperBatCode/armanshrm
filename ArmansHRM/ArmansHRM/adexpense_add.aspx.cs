﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;

namespace ArmansHRM
{
    public partial class adexpense_add : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("server=ISOMER;database=hrmdb;user id=hrmdbadmin;password=sqlserver");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"] != "Admin")
            {
                Response.Redirect("login.aspx");
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            int k = Int32.Parse(txtInitialSum.Text);
            int j = Int32.Parse(txtTravelExpense.Text);

            int ttlCost = k - j;
            txtTotalSum.Text = ttlCost.ToString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            con.Open();
            string str = "insert into expensedetails(EmployeeName,InitialSum,FinalSum,TravelExpense,TotalCost) values('" + ddlEmpId.Text + "','" + txtInitialSum.Text + "','" + txtFinalSum.Text + "','" + txtTravelExpense.Text + "','"+ txtTotalSum.Text +"')";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            con.Close();
            Label1.Text = "Values Sucessfully Added to Database, Congratulations!";
        }
    }
}