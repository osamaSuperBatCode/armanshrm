﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArmansHRM
{
    public partial class adsalary : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"] != "Admin")
            {
                Response.Redirect("login.aspx");
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            foreach(GridViewRow gvg in GridView1.Rows)
            {
                TextBox baseSalary = (TextBox)gvg.FindControl("TextBox3");
                TextBox leavesFlag = (TextBox)gvg.FindControl("TextBox4");
                TextBox finalSalaryTB = (TextBox)gvg.FindControl("Textbox5");

                float percentage = 5 / 100;
                float baseSalaryInt = float.Parse(baseSalary.Text);
                float temp;
                string leavesFlagString = leavesFlag.Text;

                if (leavesFlag.Text == "Yes")
                {
                    temp = percentage * baseSalaryInt;
                    float finalSalaryInt = baseSalaryInt - temp;
                    finalSalaryTB.Text = finalSalaryInt.ToString();
                }

                if(leavesFlag.Text == "No")
                {
                    finalSalaryTB.Text = baseSalaryInt.ToString();
                }



            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("adsalary_add.aspx");
        }
    }
}