﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="ArmansHRM.login" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Armans Photography Human Resource Management System</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

</head>

  <body class="login-body">

    <div class="container">

      <form runat="server" class="form-signin">
        <h2 class="form-signin-heading">Armans Photography</h2>
        <div class="login-wrap">
            <asp:RequiredFieldValidator ID="txtUserNameValidator" runat="server" ErrorMessage="Hey sparky, user cannot be blank!" ControlToValidate="txtUserName" InitialValue=" " CssClass="help-block" Display="Dynamic" ForeColor="#C66C42"></asp:RequiredFieldValidator>
            <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control error" placeholder="Username"></asp:TextBox>
            <asp:RequiredFieldValidator ID="txtPasswordValidator" runat="server" ErrorMessage="Shhh, What's the secret password mate?" ControlToValidate="txtPassword" InitialValue=" " CssClass="help-block" Display="Dynamic" ForeColor="#C66C42"></asp:RequiredFieldValidator><br />
            <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" placeholder="Password" TextMode="Password" ></asp:TextBox><br />
            <asp:DropDownList ID="loginSelect" runat="server" CssClass="form-control m-bot15" Height="50px">
                <asp:ListItem>Admin</asp:ListItem>
                <asp:ListItem>Employee</asp:ListItem>
            </asp:DropDownList><br />  
            <asp:Button ID="signInButton" runat="server" Text="Sign In" CssClass="btn btn-lg btn-login btn-block" OnClick="signInButton_Click" /><br />
            <asp:Label ID="wrongUPLabel" runat="server" Text="Label" Visible="False" ForeColor="#C66C42"></asp:Label>
        </div>

      </form>

    </div>



    <!-- js placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>


  </body>
</html>
