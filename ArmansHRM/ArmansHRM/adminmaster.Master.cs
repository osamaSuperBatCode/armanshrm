﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArmansHRM
{
    public partial class adminmaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = "Welcome, " + Session["username"];
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Session["type"] = null;
            Session["username"] = null;
            Response.Redirect("login.aspx");
        }
    }
}