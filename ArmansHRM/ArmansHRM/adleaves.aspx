﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adleaves.aspx.cs" Inherits="ArmansHRM.adleaves" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Leaves Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
    <form class="form-horizontal tasi-form" runat="server">

        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="SerialNumber" DataSourceID="SqlDataSource1" CssClass="table table-responsive table-bordered table-striped table-hover">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:TemplateField HeaderText="SerialNumber" InsertVisible="False" SortExpression="SerialNumber">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EmployeeName" SortExpression="EmployeeName">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="StartDateRequiredValidator" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox2" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox1" runat="server" ReadOnly="true" Text='<%# Bind("EmployeeName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LeaveTypeName" SortExpression="LeaveTypeName">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="StartDateRequiredValidator1" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox2" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("LeaveTypeName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("LeaveTypeName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="StartDate" SortExpression="StartDate">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="StartDateRequiredValidator2" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox3" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("StartDate") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("StartDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EndDate" SortExpression="EndDate">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="StartDateRequiredValidator3" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox4" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("EndDate") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("EndDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [leavesdetails] WHERE [SerialNumber] = @original_SerialNumber AND (([EmployeeName] = @original_EmployeeName) OR ([EmployeeName] IS NULL AND @original_EmployeeName IS NULL)) AND (([LeaveTypeName] = @original_LeaveTypeName) OR ([LeaveTypeName] IS NULL AND @original_LeaveTypeName IS NULL)) AND (([StartDate] = @original_StartDate) OR ([StartDate] IS NULL AND @original_StartDate IS NULL)) AND (([EndDate] = @original_EndDate) OR ([EndDate] IS NULL AND @original_EndDate IS NULL))" InsertCommand="INSERT INTO [leavesdetails] ([EmployeeName], [LeaveTypeName], [StartDate], [EndDate]) VALUES (@EmployeeName, @LeaveTypeName, @StartDate, @EndDate)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [leavesdetails]" UpdateCommand="UPDATE [leavesdetails] SET [EmployeeName] = @EmployeeName, [LeaveTypeName] = @LeaveTypeName, [StartDate] = @StartDate, [EndDate] = @EndDate WHERE [SerialNumber] = @original_SerialNumber AND (([EmployeeName] = @original_EmployeeName) OR ([EmployeeName] IS NULL AND @original_EmployeeName IS NULL)) AND (([LeaveTypeName] = @original_LeaveTypeName) OR ([LeaveTypeName] IS NULL AND @original_LeaveTypeName IS NULL)) AND (([StartDate] = @original_StartDate) OR ([StartDate] IS NULL AND @original_StartDate IS NULL)) AND (([EndDate] = @original_EndDate) OR ([EndDate] IS NULL AND @original_EndDate IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                <asp:Parameter Name="original_EmployeeName" Type="String" />
                <asp:Parameter Name="original_LeaveTypeName" Type="String" />
                <asp:Parameter Name="original_StartDate" Type="String" />
                <asp:Parameter Name="original_EndDate" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="EmployeeName" Type="String" />
                <asp:Parameter Name="LeaveTypeName" Type="String" />
                <asp:Parameter Name="StartDate" Type="String" />
                <asp:Parameter Name="EndDate" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="EmployeeName" Type="String" />
                <asp:Parameter Name="LeaveTypeName" Type="String" />
                <asp:Parameter Name="StartDate" Type="String" />
                <asp:Parameter Name="EndDate" Type="String" />
                <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                <asp:Parameter Name="original_EmployeeName" Type="String" />
                <asp:Parameter Name="original_LeaveTypeName" Type="String" />
                <asp:Parameter Name="original_StartDate" Type="String" />
                <asp:Parameter Name="original_EndDate" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>

        <br /><br /><br />
    <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Add New Value" CssClass="btn btn-danger" OnClick="Button1_Click" />
        </form>
          </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
