﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adprojects_add.aspx.cs" Inherits="ArmansHRM.adprojects_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Projects Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            <asp:Label ID="lblEmpId" runat="server" Text="Employee ID: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlEmpId" CssClass="form-control m-bot15" runat="server"  DataSourceID="hrmdbDataSource" DataTextField="FirstName" DataValueField="FirstName">
                                
                            </asp:DropDownList>
                            </div><br /><br /><br />

                            <asp:SqlDataSource ID="hrmdbDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [FirstName] FROM [personaldetails]"></asp:SqlDataSource>

                            <asp:RequiredFieldValidator ID="ProjectNameRequiredValidator" runat="server" ErrorMessage="Required!" ControlToValidate="txtProjectName" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="ProjectNameRegexValidator" ControlToValidate="txtProjectName" runat="server" ErrorMessage="Numerical name? :O" ValidationExpression="[a-z]*" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="lblProjectName" runat="server" Text="Project Name: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtProjectName" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                            <asp:Label ID="lblClient" runat="server" Text="Client: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlClient" CssClass="form-control m-bot15" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="Name">
                                
                            </asp:DropDownList>
                            </div><br /><br /><br />
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [Name] FROM [clientdetails]"></asp:SqlDataSource>

                            
                            <asp:Label ID="lblDetails" runat="server" Text="Details: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtDetails" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </div><br /><br /><br /><br />

                            <asp:Label ID="lblStatus" runat="server" Text="Status: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlStatus" CssClass="form-control m-bot15" runat="server">
                                <asp:ListItem>Active</asp:ListItem>
                                <asp:ListItem>Inactive</asp:ListItem>
                            </asp:DropDownList>
                            </div><br /><br /><br />

                            <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-info" OnClick="Button1_Click" />
                            <asp:Label ID="Label1" runat="server" Text="" CssClass="alert-success"></asp:Label>


                         </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
