﻿<%@ Page Title="" Language="C#" MasterPageFile="~/employeemaster.Master" AutoEventWireup="true" CodeBehind="emprojects.aspx.cs" Inherits="ArmansHRM.emprojects" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Projects Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                  <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">


                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="SerialNumber" DataSourceID="SqlDataSource1">
                                <Columns>
                                    <asp:BoundField DataField="SerialNumber" HeaderText="SerialNumber" InsertVisible="False" ReadOnly="True" SortExpression="SerialNumber" />
                                    <asp:BoundField DataField="EmployeeName" HeaderText="EmployeeName" SortExpression="EmployeeName" />
                                    <asp:BoundField DataField="NameOfProject" HeaderText="NameOfProject" SortExpression="NameOfProject" />
                                    <asp:BoundField DataField="ClientName" HeaderText="ClientName" SortExpression="ClientName" />
                                    <asp:BoundField DataField="Details" HeaderText="Details" SortExpression="Details" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                                </Columns>
                            </asp:GridView>

                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT * FROM [projectdetails]"></asp:SqlDataSource>

                            <br /><br /><br />
                            <div class="col-sm-5"></div>
                            

                        </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
