﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adexpense_add.aspx.cs" Inherits="ArmansHRM.adexpense_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Expense Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            <asp:Label ID="lblEmpId" runat="server" Text="Employee ID: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlEmpId" CssClass="form-control m-bot15" runat="server" DataSourceID="SqlDataSource1" DataTextField="FirstName" DataValueField="FirstName">
                                
                            </asp:DropDownList>
                            </div><br /><br /><br />
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [FirstName] FROM [personaldetails]"></asp:SqlDataSource>
                          
                            <asp:RequiredFieldValidator ID="InitialSumRequiredValidator" runat="server" ErrorMessage="Required Initial Sum!" ControlToValidate="txtInitialSum" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="InitialSumRegexValidator" ControlToValidate="txtInitialSum" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="lblInitialSum" runat="server" Text="Initial Sum: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtInitialSum" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="FinalSumRequiredValidator" runat="server" ErrorMessage="Required Final Sum!" ControlToValidate="txtFinalSum" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="FinalSumRegexValidator" ControlToValidate="txtFinalSum" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="lblFinalSum" runat="server" Text="Final Sum: " CssClass="control-label col-sm-2"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtFinalSum" runat="server" CssClass="form-control"></asp:TextBox>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="TravelExpenseRequiredValidator" runat="server" ErrorMessage="Required Travel Expense!" ControlToValidate="txtTravelExpense" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="TravelExpenseRegexValidator" ControlToValidate="txtTravelExpense" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="lblTravelExpense" runat="server" Text="Travel Expense: " CssClass="control-label col-sm-2"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtTravelExpense" runat="server" CssClass="form-control"></asp:TextBox>
                            </div><br /><br /><br />

                            <asp:Label ID="lblTotalSum" runat="server" Text="Total Sum: " CssClass="control-label col-sm-2"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtTotalSum" runat="server" CssClass="form-control" onfocus="blur()"></asp:TextBox>
                            </div><br /><br /><br />

                            

                            
                            <asp:Button ID="Button2" runat="server" Text="Calculate" CssClass="btn btn-warning" OnClick="Button2_Click" />

                            <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-info" OnClick="Button1_Click" />
                            <asp:Label ID="Label1" runat="server" Text="" CssClass="alert-success"></asp:Label>

                            
                            
                         </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
