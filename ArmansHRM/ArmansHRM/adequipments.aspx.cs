﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArmansHRM
{
    public partial class adequipments : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"] != "Admin")
            {
                Response.Redirect("login.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("adequipments_add.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            foreach(GridViewRow gvr in GridView1.Rows)
            {
                TextBox costOneDay = (TextBox)gvr.FindControl("TextBox3");
                TextBox costNDays= (TextBox)gvr.FindControl("TextBox4");
                TextBox serviceTaxCal = (TextBox)gvr.FindControl("Textbox5");
                TextBox finalSumation = (TextBox)gvr.FindControl("Textbox6");

                int costOneDayInt = int.Parse(costOneDay.Text);
                int costNDayInt = int.Parse(costNDays.Text);
                float serviceTaxInt = float.Parse(serviceTaxCal.Text) / 100;

                int finalCalc = costOneDayInt * costNDayInt;
                float finalwithST = finalCalc + ( finalCalc * serviceTaxInt );

                finalSumation.Text = finalwithST.ToString();


                

            }
        }
    }
}