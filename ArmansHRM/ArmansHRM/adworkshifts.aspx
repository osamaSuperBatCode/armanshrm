﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adworkshifts.aspx.cs" Inherits="ArmansHRM.adworkshifts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Workshifts Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
    <form class="form-horizontal tasi-form" runat="server">       
    
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="SerialNumber" DataSourceID="SqlDataSource1" CssClass="table table-responsive table-striped table-hover table-bordered">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:TemplateField HeaderText="SerialNumber" InsertVisible="False" SortExpression="SerialNumber">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EmployeeName" SortExpression="EmployeeName">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" ReadOnly="true" Text='<%# Bind("EmployeeName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Type" SortExpression="Type">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="WorkshiftTypeRequiredValidator" runat="server" ErrorMessage="Required Workshift Type!" ControlToValidate="TextBox2" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Type") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Duration" SortExpression="Duration">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="Durationvalidator" runat="server" ErrorMessage="Required Workshift Type!" ControlToValidate="TextBox3" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="DurationRegexValidator" ControlToValidate="TextBox3" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Duration") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("Duration") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    
    
    
    
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [workshiftdetails] WHERE [SerialNumber] = @original_SerialNumber AND (([EmployeeName] = @original_EmployeeName) OR ([EmployeeName] IS NULL AND @original_EmployeeName IS NULL)) AND (([Type] = @original_Type) OR ([Type] IS NULL AND @original_Type IS NULL)) AND (([Duration] = @original_Duration) OR ([Duration] IS NULL AND @original_Duration IS NULL))" InsertCommand="INSERT INTO [workshiftdetails] ([EmployeeName], [Type], [Duration]) VALUES (@EmployeeName, @Type, @Duration)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [workshiftdetails]" UpdateCommand="UPDATE [workshiftdetails] SET [EmployeeName] = @EmployeeName, [Type] = @Type, [Duration] = @Duration WHERE [SerialNumber] = @original_SerialNumber AND (([EmployeeName] = @original_EmployeeName) OR ([EmployeeName] IS NULL AND @original_EmployeeName IS NULL)) AND (([Type] = @original_Type) OR ([Type] IS NULL AND @original_Type IS NULL)) AND (([Duration] = @original_Duration) OR ([Duration] IS NULL AND @original_Duration IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                <asp:Parameter Name="original_EmployeeName" Type="String" />
                <asp:Parameter Name="original_Type" Type="String" />
                <asp:Parameter Name="original_Duration" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="EmployeeName" Type="String" />
                <asp:Parameter Name="Type" Type="String" />
                <asp:Parameter Name="Duration" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="EmployeeName" Type="String" />
                <asp:Parameter Name="Type" Type="String" />
                <asp:Parameter Name="Duration" Type="String" />
                <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                <asp:Parameter Name="original_EmployeeName" Type="String" />
                <asp:Parameter Name="original_Type" Type="String" />
                <asp:Parameter Name="original_Duration" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>
    
    
    
    
    <br /><br /><br />
    <div class="col-sm-5"></div>
        <asp:Button ID="Button1" runat="server" Text="Add New Value" CssClass="btn btn-danger" OnClick="Button1_Click" />
        </form>
          </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
