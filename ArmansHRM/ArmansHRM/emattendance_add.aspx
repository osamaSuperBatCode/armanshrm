﻿<%@ Page Title="" Language="C#" MasterPageFile="~/employeemaster.Master" AutoEventWireup="true" CodeBehind="emattendance_add.aspx.cs" Inherits="ArmansHRM.emattendance_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Personal Information Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            
                          <asp:Label ID="lblEmpId" runat="server" Text="Employee ID: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                                <asp:DropDownList ID="ddlEmpId" runat="server" CssClass="form-control" DataSourceID="SqlDataSource1" DataTextField="FirstName" DataValueField="FirstName">

                                </asp:DropDownList>
                            </div><br /><br /><br />

                            <asp:Label ID="lblPunchInTime" runat="server" Text="Punch In Timing: " CssClass="col-sm-2 control-label"></asp:Label>
                            <div class ="col-sm-8 input-group bootstrap-timepicker">
                                <asp:TextBox ID="txtPunchInTime" runat="server" onfocus="blur()" CssClass="form-control timepicker-default"></asp:TextBox>
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="icon-time"></i></button>
                                </span>
                            <asp:RequiredFieldValidator ID="PunchInTimeRequiredValidator" runat="server" ErrorMessage="Required Time!" ControlToValidate="txtPunchInTime" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block"></asp:RequiredFieldValidator>
                            </div><br /><br /><br />
                            
                            <asp:RequiredFieldValidator ID="PunchOutTimeRequiredValidator" runat="server" ErrorMessage="Required Time!" ControlToValidate="txtPunchOutTime" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblPunchOutTime" runat="server" Text="Punch Out Timing: " CssClass="col-sm-2 control-label"></asp:Label>
                            <div class ="col-sm-8 input-group bootstrap-timepicker">
                                <asp:TextBox ID="txtPunchOutTime" runat="server" onfocus="blur()" CssClass="form-control timepicker-default"></asp:TextBox>
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><i class="icon-time"></i></button>
                                </span>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="DateRequiredValidator" runat="server" ErrorMessage="Required Date!" ControlToValidate="txtDate" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblDate" runat="server" Text="Date: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8 input-group date form_datetime-component">
                            <asp:TextBox ID="txtDate" onfocus="blur()" runat="server" CssClass="form_datetime form-control" ></asp:TextBox>
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-danger date-reset"><i class="icon-remove"></i></button>
                            <button type="button" class="btn btn-warning date-set"><i class="icon-calendar"></i></button>
                            </span>
                            </div><br /><br /><br />

                            <asp:Label ID="lblNotes" runat="server" Text="Additional Notes: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtNotes" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </div><br /><br /><br /><br />

                             <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [FirstName] FROM [personaldetails]"></asp:SqlDataSource>

                            <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-info" OnClick="Button1_Click" />
                            <asp:Label ID="Label1" runat="server" Text="" CssClass="alert-success"></asp:Label>

                         </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
