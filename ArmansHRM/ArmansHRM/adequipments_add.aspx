﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adequipments_add.aspx.cs" Inherits="ArmansHRM.adequipments_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Equipments Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            
                          
                            <asp:Label ID="lblEquipmentName" runat="server" Text="Equipment Name: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtEquipmentName" runat="server" CssClass="form-control" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="EquipmentNameRequiredValidator" runat="server" ErrorMessage="Required Equipment!" ControlToValidate="txtEquipmentName" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="EquipmentGivenRequiredValidator" runat="server" ErrorMessage="Equipment Required!" ControlToValidate="txtEquipmentGiven" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblEquipmentGiven" runat="server" Text="Equipment Given To: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtEquipmentGiven" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="CostOneDayRequiredValidator" runat="server" ErrorMessage="Amount Required!" ControlToValidate="txtCostOneDay" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="CostOneDaysRegexValidator" ControlToValidate="txtCostOneDay" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="lblGivenForAmount" runat="server" Text="Cost For One Day: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtCostOneDay" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                             
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtCostNDay" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="Label2" runat="server" Text="Number Of Days: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtCostNDay" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Amount Required!" ControlToValidate="txtServiceTax" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="txtServiceTax" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="Label3" runat="server" Text="Service Tax Amount: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtServiceTax" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                            
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ControlToValidate="txtFinalSum" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="Label4" runat="server" Text="Final Sum: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtFinalSum" onfocus="blur()" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />



                            <div class="col-sm-5"></div>
                            <asp:Button ID="Button2" runat="server" Text="Calculate" CssClass="btn btn-warning" OnClick="Button2_Click" />
                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-info" OnClick="Button1_Click" /><br /><br /><br />
                            <div class="col-sm-4"></div>
                            <asp:Label ID="Label1" runat="server" Text="" CssClass="alert-success"></asp:Label>

                         </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
