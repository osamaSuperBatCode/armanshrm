﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adexpense.aspx.cs" Inherits="ArmansHRM.adexpense" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Expense Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="SerialNumber" DataSourceID="SqlDataSource1" CssClass="table table-responsive table-hover table-bordered table-striped table-advance">
                                <Columns>
                                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True"/>
                                    <asp:TemplateField HeaderText="SerialNumber" InsertVisible="False" SortExpression="SerialNumber">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EmployeeName" SortExpression="EmployeeName">
                                        <EditItemTemplate>
                                            <asp:RequiredFieldValidator ID="ExpenseValidator" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox1" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="ExpenseValidatorRegex" ControlToValidate="TextBox1" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                                            <asp:TextBox ID="TextBox1" runat="server" onfocus="blur()" Text='<%# Bind("EmployeeName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="InitialSum" SortExpression="InitialSum">
                                        <EditItemTemplate>
                                        <asp:RequiredFieldValidator ID="ExpenseValidator2" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox2" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="ExpenseValidatorRegex2" ControlToValidate="TextBox2" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("InitialSum") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("InitialSum") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="FinalSum" SortExpression="FinalSum">
                                        <EditItemTemplate>
                                <asp:RequiredFieldValidator ID="ExpenseValidator3" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox3" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="ExpenseValidatorRegex3" ControlToValidate="TextBox3" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                                            <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("FinalSum") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("FinalSum") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TravelExpense" SortExpression="TravelExpense">
                                        <EditItemTemplate>
                                                                                        <asp:RequiredFieldValidator ID="ExpenseValidator4" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox4" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="ExpenseValidatorRegex4" ControlToValidate="TextBox4" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("TravelExpense") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("TravelExpense") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TotalCost" SortExpression="TotalCost">
                                        <EditItemTemplate>
                                                                                        <asp:RequiredFieldValidator ID="ExpenseValidator5" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox5" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="ExpenseValidatorRegex5" ControlToValidate="TextBox5" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                                            <asp:TextBox ID="TextBox5" runat="server" onfocus="blur()" Text='<%# Bind("TotalCost") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("TotalCost") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [expensedetails] WHERE [SerialNumber] = @original_SerialNumber AND (([EmployeeName] = @original_EmployeeName) OR ([EmployeeName] IS NULL AND @original_EmployeeName IS NULL)) AND (([InitialSum] = @original_InitialSum) OR ([InitialSum] IS NULL AND @original_InitialSum IS NULL)) AND (([FinalSum] = @original_FinalSum) OR ([FinalSum] IS NULL AND @original_FinalSum IS NULL)) AND (([TravelExpense] = @original_TravelExpense) OR ([TravelExpense] IS NULL AND @original_TravelExpense IS NULL)) AND (([TotalCost] = @original_TotalCost) OR ([TotalCost] IS NULL AND @original_TotalCost IS NULL))" InsertCommand="INSERT INTO [expensedetails] ([EmployeeName], [InitialSum], [FinalSum], [TravelExpense], [TotalCost]) VALUES (@EmployeeName, @InitialSum, @FinalSum, @TravelExpense, @TotalCost)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [expensedetails]" UpdateCommand="UPDATE [expensedetails] SET [EmployeeName] = @EmployeeName, [InitialSum] = @InitialSum, [FinalSum] = @FinalSum, [TravelExpense] = @TravelExpense, [TotalCost] = @TotalCost WHERE [SerialNumber] = @original_SerialNumber AND (([EmployeeName] = @original_EmployeeName) OR ([EmployeeName] IS NULL AND @original_EmployeeName IS NULL)) AND (([InitialSum] = @original_InitialSum) OR ([InitialSum] IS NULL AND @original_InitialSum IS NULL)) AND (([FinalSum] = @original_FinalSum) OR ([FinalSum] IS NULL AND @original_FinalSum IS NULL)) AND (([TravelExpense] = @original_TravelExpense) OR ([TravelExpense] IS NULL AND @original_TravelExpense IS NULL)) AND (([TotalCost] = @original_TotalCost) OR ([TotalCost] IS NULL AND @original_TotalCost IS NULL))" >
                                <DeleteParameters>
                                    <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                                    <asp:Parameter Name="original_EmployeeName" Type="String" />
                                    <asp:Parameter Name="original_InitialSum" Type="String" />
                                    <asp:Parameter Name="original_FinalSum" Type="String" />
                                    <asp:Parameter Name="original_TravelExpense" Type="String" />
                                    <asp:Parameter Name="original_TotalCost" Type="String" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="EmployeeName" Type="String" />
                                    <asp:Parameter Name="InitialSum" Type="String" />
                                    <asp:Parameter Name="FinalSum" Type="String" />
                                    <asp:Parameter Name="TravelExpense" Type="String" />
                                    <asp:Parameter Name="TotalCost" Type="String" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="EmployeeName" Type="String" />
                                    <asp:Parameter Name="InitialSum" Type="String" />
                                    <asp:Parameter Name="FinalSum" Type="String" />
                                    <asp:Parameter Name="TravelExpense" Type="String" />
                                    <asp:Parameter Name="TotalCost" Type="String" />
                                    <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                                    <asp:Parameter Name="original_EmployeeName" Type="String" />
                                    <asp:Parameter Name="original_InitialSum" Type="String" />
                                    <asp:Parameter Name="original_FinalSum" Type="String" />
                                    <asp:Parameter Name="original_TravelExpense" Type="String" />
                                    <asp:Parameter Name="original_TotalCost" Type="String" />
                                </UpdateParameters>
                            </asp:SqlDataSource>

                             <br /><br /><br />
                            <asp:Button ID="Button2" runat="server" Text="Calculate" CssClass="btn btn-warning" OnClick="Button2_Click" />
    <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Add New Value" CssClass="btn btn-danger" OnClick="Button1_Click" />

                            
        </form>

                        
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
