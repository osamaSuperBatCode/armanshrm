﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adpersonalinfo_add.aspx.cs" Inherits="ArmansHRM.adpersonalinfo_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Personal Information Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            <asp:Label ID="lblUserName" runat="server" Text="Username: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtUserName" runat="server" ReadOnly="true" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />
                            
                            <asp:RequiredFieldValidator ID="FullNameRequiredValidator" runat="server" ErrorMessage="Every person has a name..." ControlToValidate="txtFullName" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="FullNameRegexValidator" ControlToValidate="txtFullName" runat="server" ErrorMessage="Numerical name? :O" ValidationExpression="^([a-zA-Z]+(_[a-zA-Z]+)*)(\s([a-zA-Z]+(_[a-zA-Z]+)*))*$" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="lblFullName" runat="server" Text="Full Name: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtFullName" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />
                            
                            <asp:RequiredFieldValidator ID="DOBRequiredValidator" runat="server" ErrorMessage="Enter Birthday Please!" ControlToValidate="txtDOB" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblDOB" runat="server" Text="Date Of Birth: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8 input-group date form_datetime-component">
                            <asp:TextBox ID="txtDOB" ReadOnly="true" runat="server" CssClass="form_datetime form-control" ></asp:TextBox>
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-danger date-reset"><i class="icon-remove"></i></button>
                            <button type="button" class="btn btn-warning date-set"><i class="icon-calendar"></i></button>
                            </span>
                            </div><br /><br /><br />

                            <asp:Label ID="lblGender" runat="server" Text="Gender: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlGender" CssClass="form-control m-bot15" runat="server">
                                <asp:ListItem>Male</asp:ListItem>
                                <asp:ListItem>Female</asp:ListItem>
                            </asp:DropDownList>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="DepartmentRequiredValidator" runat="server" ErrorMessage="Department a must" ControlToValidate="txtDepartment" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblDepartment" runat="server" Text="Department: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtDepartment" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                            <asp:Label ID="lblEmpStat" runat="server" Text="Employee Status: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlEmpStat" CssClass="form-control m-bot15" runat="server">
                                <asp:ListItem>Full Time</asp:ListItem>
                                <asp:ListItem>Part Time</asp:ListItem>
                            </asp:DropDownList>
                            </div><br /><br /><br />

                            <asp:Label ID="lblPayGrade" runat="server" Text="Paygrade: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlPayGrade" CssClass="form-control m-bot15" runat="server">
                                
                            </asp:DropDownList>
                            </div><br /><br /><br />

                            <asp:Label ID="lblSupervisor" runat="server" Text="Supervisor: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlSupervisor" CssClass="form-control m-bot15" runat="server">
                                
                            </asp:DropDownList>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Unqualified Employee?" ControlToValidate="txtQualifications" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblQualifications" runat="server" Text="Qualifications: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtQualifications" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                            <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-info" />

                        </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
