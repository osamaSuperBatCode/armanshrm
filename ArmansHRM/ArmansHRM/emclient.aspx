﻿<%@ Page Title="" Language="C#" MasterPageFile="~/employeemaster.Master" AutoEventWireup="true" CodeBehind="emclient.aspx.cs" Inherits="ArmansHRM.emclient" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Clients Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                  <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="SerialNumber" DataSourceID="SqlDataSource1">
                                <Columns>
                                    <asp:BoundField DataField="SerialNumber" HeaderText="SerialNumber" InsertVisible="False" ReadOnly="True" SortExpression="SerialNumber" />
                                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                                    <asp:BoundField DataField="ContactNumber" HeaderText="ContactNumber" SortExpression="ContactNumber" />
                                    <asp:BoundField DataField="ClientEmail" HeaderText="ClientEmail" SortExpression="ClientEmail" />
                                    <asp:BoundField DataField="Details" HeaderText="Details" SortExpression="Details" />
                                    <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                                </Columns>
                            </asp:GridView>


                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT * FROM [clientdetails]"></asp:SqlDataSource>


                        <br /><br /><br />
                            <div class="col-sm-5"></div>
                            

                        </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
