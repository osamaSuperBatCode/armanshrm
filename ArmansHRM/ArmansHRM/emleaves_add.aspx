﻿<%@ Page Title="" Language="C#" MasterPageFile="~/employeemaster.Master" AutoEventWireup="true" CodeBehind="emleaves_add.aspx.cs" Inherits="ArmansHRM.emleaves_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Leaves Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            <asp:Label ID="lblEmpId" runat="server" Text="Employee ID: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlEmpId" CssClass="form-control m-bot15" runat="server" DataSourceID="SqlDataSource1" DataTextField="FirstName" DataValueField="FirstName">
                                
                            </asp:DropDownList>
                            </div><br /><br /><br />
                          
                            <asp:RequiredFieldValidator ID="LeaveTypeRequiredValidator" runat="server" ErrorMessage="Required Leave Type Name!" ControlToValidate="txtLeaveType" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type Name: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtLeaveType" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="StartDateRequiredValidator" runat="server" ErrorMessage="Enter StartDate Please!" ControlToValidate="txtStartDate" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8 input-group date form_datetime-component">
                            <asp:TextBox ID="txtStartdate" onfocus="blur()" runat="server" CssClass="form_datetime form-control" ></asp:TextBox>
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-danger date-reset"><i class="icon-remove"></i></button>
                            <button type="button" class="btn btn-warning date-set"><i class="icon-calendar"></i></button>
                            </span>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="EndDateRequiredValidator" runat="server" ErrorMessage="Enter EndDate Please!" ControlToValidate="txtEndDate" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblEndDate" runat="server" Text="End Date: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8 input-group date form_datetime-component">
                            <asp:TextBox ID="txtEndDate" onfocus="blur()" runat="server" CssClass="form_datetime form-control" ></asp:TextBox>
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-danger date-reset"><i class="icon-remove"></i></button>
                            <button type="button" class="btn btn-warning date-set"><i class="icon-calendar"></i></button>
                            </span>
                            </div><br /><br /><br />

                             <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [FirstName] FROM [personaldetails]"></asp:SqlDataSource>

                            <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-info" OnClick="Button1_Click" />
                            
                            <asp:Label ID="Label1" runat="server" Text="" CssClass="alert-success"></asp:Label>

                         </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
