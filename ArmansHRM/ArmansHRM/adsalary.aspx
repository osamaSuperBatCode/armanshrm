﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adsalary.aspx.cs" Inherits="ArmansHRM.adsalary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Salary Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
    <form class="form-horizontal tasi-form" runat="server">
        
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="SerialNumber" DataSourceID="SqlDataSource1" CssClass="table table-bordered table-hover table-striped table-responsive">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:TemplateField HeaderText="SerialNumber" InsertVisible="False" SortExpression="SerialNumber">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EmployeeName" SortExpression="EmployeeName">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="EmployeenameRequired" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox1" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PaygradeName" SortExpression="PaygradeName">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="pgreq" runat="server" ErrorMessage="Required Paygrade Name!" ControlToValidate="TextBox2" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("PaygradeName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("PaygradeName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Salary" SortExpression="Salary">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="SalaryRequiredValidator2" runat="server" ErrorMessage="Required Salary!" ControlToValidate="TextBox3" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="SalaryRegexValidator3" ControlToValidate="TextBox3" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("Salary") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("Salary") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LeavesReduction" SortExpression="LeavesReduction">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="SalaryRequiredValidator4" runat="server" ErrorMessage="Required Reduction!" ControlToValidate="TextBox4" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("LeavesReduction") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("LeavesReduction") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FinalSalary" SortExpression="FinalSalary">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox5" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="TextBox5" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                        <asp:TextBox ID="TextBox5" onfocus="blur()" runat="server" Text='<%# Bind("FinalSalary") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("FinalSalary") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>


        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [salarydetails] WHERE [SerialNumber] = @original_SerialNumber AND (([EmployeeName] = @original_EmployeeName) OR ([EmployeeName] IS NULL AND @original_EmployeeName IS NULL)) AND (([PaygradeName] = @original_PaygradeName) OR ([PaygradeName] IS NULL AND @original_PaygradeName IS NULL)) AND (([Salary] = @original_Salary) OR ([Salary] IS NULL AND @original_Salary IS NULL)) AND (([LeavesReduction] = @original_LeavesReduction) OR ([LeavesReduction] IS NULL AND @original_LeavesReduction IS NULL)) AND (([FinalSalary] = @original_FinalSalary) OR ([FinalSalary] IS NULL AND @original_FinalSalary IS NULL))" InsertCommand="INSERT INTO [salarydetails] ([EmployeeName], [PaygradeName], [Salary], [LeavesReduction], [FinalSalary]) VALUES (@EmployeeName, @PaygradeName, @Salary, @LeavesReduction, @FinalSalary)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [salarydetails]" UpdateCommand="UPDATE [salarydetails] SET [EmployeeName] = @EmployeeName, [PaygradeName] = @PaygradeName, [Salary] = @Salary, [LeavesReduction] = @LeavesReduction, [FinalSalary] = @FinalSalary WHERE [SerialNumber] = @original_SerialNumber AND (([EmployeeName] = @original_EmployeeName) OR ([EmployeeName] IS NULL AND @original_EmployeeName IS NULL)) AND (([PaygradeName] = @original_PaygradeName) OR ([PaygradeName] IS NULL AND @original_PaygradeName IS NULL)) AND (([Salary] = @original_Salary) OR ([Salary] IS NULL AND @original_Salary IS NULL)) AND (([LeavesReduction] = @original_LeavesReduction) OR ([LeavesReduction] IS NULL AND @original_LeavesReduction IS NULL)) AND (([FinalSalary] = @original_FinalSalary) OR ([FinalSalary] IS NULL AND @original_FinalSalary IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                <asp:Parameter Name="original_EmployeeName" Type="String" />
                <asp:Parameter Name="original_PaygradeName" Type="String" />
                <asp:Parameter Name="original_Salary" Type="String" />
                <asp:Parameter Name="original_LeavesReduction" Type="String" />
                <asp:Parameter Name="original_FinalSalary" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="EmployeeName" Type="String" />
                <asp:Parameter Name="PaygradeName" Type="String" />
                <asp:Parameter Name="Salary" Type="String" />
                <asp:Parameter Name="LeavesReduction" Type="String" />
                <asp:Parameter Name="FinalSalary" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="EmployeeName" Type="String" />
                <asp:Parameter Name="PaygradeName" Type="String" />
                <asp:Parameter Name="Salary" Type="String" />
                <asp:Parameter Name="LeavesReduction" Type="String" />
                <asp:Parameter Name="FinalSalary" Type="String" />
                <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                <asp:Parameter Name="original_EmployeeName" Type="String" />
                <asp:Parameter Name="original_PaygradeName" Type="String" />
                <asp:Parameter Name="original_Salary" Type="String" />
                <asp:Parameter Name="original_LeavesReduction" Type="String" />
                <asp:Parameter Name="original_FinalSalary" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>


        <br /><br /><br />
    <div class="col-sm-5"></div>
        <asp:Button ID="Button2" runat="server" Text="Calculate" CssClass="btn btn-warning" OnClick="Button2_Click" />
        <asp:Button ID="Button1" runat="server" Text="Add New Value" CssClass="btn btn-danger" OnClick="Button1_Click" />
        </form>
          </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
