﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adpaygrade.aspx.cs" Inherits="ArmansHRM.adpaygrade" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Paygrade Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
    <form class="form-horizontal tasi-form" runat="server">

        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="SerialNumber" DataSourceID="SqlDataSource1" CssClass="table table-bordered table-striped table-hover table-responsive">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:TemplateField HeaderText="SerialNumber" InsertVisible="False" SortExpression="SerialNumber">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NameOfPaygrade" SortExpression="NameOfPaygrade">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="PaygradeNameRequiredValidator" runat="server" ErrorMessage="Required Paygrade Name!" ControlToValidate="TextBox1" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("NameOfPaygrade") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("NameOfPaygrade") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="MinSalary" SortExpression="MinSalary">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="MinSalaryRequiredValidator" runat="server" ErrorMessage="Minimum Salary Required!" ControlToValidate="TextBox2" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="MinimumSalaryRegexValidator" ControlToValidate="TextBox2" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("MinSalary") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("MinSalary") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="MaxSalary" SortExpression="MaxSalary">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="MaximumSalaryRequiredValidator" runat="server" ErrorMessage="Maximum Salary Required!" ControlToValidate="TextBox3" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="MaximumSalaryRegexValidator" ControlToValidate="TextBox3" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("MaxSalary") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("MaxSalary") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>


        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [paygradedetails] WHERE [SerialNumber] = @original_SerialNumber AND (([NameOfPaygrade] = @original_NameOfPaygrade) OR ([NameOfPaygrade] IS NULL AND @original_NameOfPaygrade IS NULL)) AND (([MinSalary] = @original_MinSalary) OR ([MinSalary] IS NULL AND @original_MinSalary IS NULL)) AND (([MaxSalary] = @original_MaxSalary) OR ([MaxSalary] IS NULL AND @original_MaxSalary IS NULL))" InsertCommand="INSERT INTO [paygradedetails] ([NameOfPaygrade], [MinSalary], [MaxSalary]) VALUES (@NameOfPaygrade, @MinSalary, @MaxSalary)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [paygradedetails]" UpdateCommand="UPDATE [paygradedetails] SET [NameOfPaygrade] = @NameOfPaygrade, [MinSalary] = @MinSalary, [MaxSalary] = @MaxSalary WHERE [SerialNumber] = @original_SerialNumber AND (([NameOfPaygrade] = @original_NameOfPaygrade) OR ([NameOfPaygrade] IS NULL AND @original_NameOfPaygrade IS NULL)) AND (([MinSalary] = @original_MinSalary) OR ([MinSalary] IS NULL AND @original_MinSalary IS NULL)) AND (([MaxSalary] = @original_MaxSalary) OR ([MaxSalary] IS NULL AND @original_MaxSalary IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                <asp:Parameter Name="original_NameOfPaygrade" Type="String" />
                <asp:Parameter Name="original_MinSalary" Type="String" />
                <asp:Parameter Name="original_MaxSalary" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="NameOfPaygrade" Type="String" />
                <asp:Parameter Name="MinSalary" Type="String" />
                <asp:Parameter Name="MaxSalary" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="NameOfPaygrade" Type="String" />
                <asp:Parameter Name="MinSalary" Type="String" />
                <asp:Parameter Name="MaxSalary" Type="String" />
                <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                <asp:Parameter Name="original_NameOfPaygrade" Type="String" />
                <asp:Parameter Name="original_MinSalary" Type="String" />
                <asp:Parameter Name="original_MaxSalary" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>


    <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Add New Value" CssClass="btn btn-danger" OnClick="Button1_Click" />
        </form>
          </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
