﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArmansHRM
{
    public partial class adexpense : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"] != "Admin")
            {
                Response.Redirect("login.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("adexpense_add.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {



            foreach (GridViewRow gvr in GridView1.Rows)
            {
                TextBox initSum = (TextBox)gvr.FindControl("TextBox2");
                TextBox travelExpense = (TextBox)gvr.FindControl("TextBox4");
                TextBox totalSum = (TextBox)gvr.FindControl("Textbox5");

                int k = int.Parse(initSum.Text);
                int j = int.Parse(travelExpense.Text);

                int ttlCost = k - j;
                totalSum.Text = ttlCost.ToString();
            }

           

        }

       
    }
}