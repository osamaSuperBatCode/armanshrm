﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adprojects.aspx.cs" Inherits="ArmansHRM.adprojects" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Projects Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
    <form class="form-horizontal tasi-form" runat="server">


        <asp:GridView ID="GridView1" runat="server" CssClass="table table-striped table-hover table-bordered" AutoGenerateColumns="False" DataKeyNames="SerialNumber" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:TemplateField HeaderText="SerialNumber" InsertVisible="False" SortExpression="SerialNumber">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EmployeeName" SortExpression="EmployeeName">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="Empnamereq" runat="server" ErrorMessage="Every person has a name..." ControlToValidate="TextBox1" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox1" onfocus="blur()" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2"  runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NameOfProject" SortExpression="NameOfProject">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="NOPreq" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox2" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("NameOfProject") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("NameOfProject") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ClientName" SortExpression="ClientName">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="clnreq" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox3" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("ClientName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("ClientName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Details" SortExpression="Details">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Details") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("Details") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="statusreq" runat="server" ErrorMessage="Required!" ControlToValidate="TextBox5" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("Status") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>

        </asp:GridView>

           <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [projectdetails] WHERE [SerialNumber] = @original_SerialNumber AND (([EmployeeName] = @original_EmployeeName) OR ([EmployeeName] IS NULL AND @original_EmployeeName IS NULL)) AND (([NameOfProject] = @original_NameOfProject) OR ([NameOfProject] IS NULL AND @original_NameOfProject IS NULL)) AND (([ClientName] = @original_ClientName) OR ([ClientName] IS NULL AND @original_ClientName IS NULL)) AND (([Details] = @original_Details) OR ([Details] IS NULL AND @original_Details IS NULL)) AND (([Status] = @original_Status) OR ([Status] IS NULL AND @original_Status IS NULL))" InsertCommand="INSERT INTO [projectdetails] ([EmployeeName], [NameOfProject], [ClientName], [Details], [Status]) VALUES (@EmployeeName, @NameOfProject, @ClientName, @Details, @Status)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [projectdetails]" UpdateCommand="UPDATE [projectdetails] SET [EmployeeName] = @EmployeeName, [NameOfProject] = @NameOfProject, [ClientName] = @ClientName, [Details] = @Details, [Status] = @Status WHERE [SerialNumber] = @original_SerialNumber AND (([EmployeeName] = @original_EmployeeName) OR ([EmployeeName] IS NULL AND @original_EmployeeName IS NULL)) AND (([NameOfProject] = @original_NameOfProject) OR ([NameOfProject] IS NULL AND @original_NameOfProject IS NULL)) AND (([ClientName] = @original_ClientName) OR ([ClientName] IS NULL AND @original_ClientName IS NULL)) AND (([Details] = @original_Details) OR ([Details] IS NULL AND @original_Details IS NULL)) AND (([Status] = @original_Status) OR ([Status] IS NULL AND @original_Status IS NULL))">
               <DeleteParameters>
                   <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                   <asp:Parameter Name="original_EmployeeName" Type="String" />
                   <asp:Parameter Name="original_NameOfProject" Type="String" />
                   <asp:Parameter Name="original_ClientName" Type="String" />
                   <asp:Parameter Name="original_Details" Type="String" />
                   <asp:Parameter Name="original_Status" Type="String" />
               </DeleteParameters>
               <InsertParameters>
                   <asp:Parameter Name="EmployeeName" Type="String" />
                   <asp:Parameter Name="NameOfProject" Type="String" />
                   <asp:Parameter Name="ClientName" Type="String" />
                   <asp:Parameter Name="Details" Type="String" />
                   <asp:Parameter Name="Status" Type="String" />
               </InsertParameters>
               <UpdateParameters>
                   <asp:Parameter Name="EmployeeName" Type="String" />
                   <asp:Parameter Name="NameOfProject" Type="String" />
                   <asp:Parameter Name="ClientName" Type="String" />
                   <asp:Parameter Name="Details" Type="String" />
                   <asp:Parameter Name="Status" Type="String" />
                   <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                   <asp:Parameter Name="original_EmployeeName" Type="String" />
                   <asp:Parameter Name="original_NameOfProject" Type="String" />
                   <asp:Parameter Name="original_ClientName" Type="String" />
                   <asp:Parameter Name="original_Details" Type="String" />
                   <asp:Parameter Name="original_Status" Type="String" />
               </UpdateParameters>
        </asp:SqlDataSource>

           <br /><br /><br />
    <div class="col-sm-5"></div>
        <asp:Button ID="Button1" runat="server" Text="Add New Value" CssClass="btn btn-danger" OnClick="Button1_Click" />
        </form>
          </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
