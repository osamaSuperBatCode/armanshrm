﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;

namespace ArmansHRM
{
    public partial class adsalary_add : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("server=ISOMER;database=hrmdb;user id=hrmdbadmin;password=sqlserver");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"] != "Admin")
            {
                Response.Redirect("login.aspx");
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if(ddlLeavesDeduction.SelectedItem.Value == "Yes")
            {
                float k = 5 / 100;
                float sal = float.Parse(txtSalary.Text);

                float reductionSal = sal * k;
                float finalSal = sal - reductionSal;

                TextBox1.Text = finalSal.ToString();
            }
            if(ddlLeavesDeduction.SelectedItem.Value == "No")
            {
                TextBox1.Text = txtSalary.Text;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            con.Open();
            string str = "insert into salarydetails(EmployeeName,PaygradeName,Salary,LeavesReduction,FinalSalary) values('" + ddlEmpId.Text + "','" + ddlPaygradeName.Text + "','" + txtSalary.Text + "','" + ddlLeavesDeduction.Text + "','" + TextBox1.Text + "')";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            con.Close();
            Label2.Text = "Values Sucessfully Added to Database, Congratulations!";
        }
    }
}