﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;

namespace ArmansHRM
{
    public partial class adequipments_add : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection("server=ISOMER;database=hrmdb;user id=hrmdbadmin;password=sqlserver");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"] != "Admin")
            {
                Response.Redirect("login.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            con.Open();
            string str = "insert into equipmentdetails(EquipmentName,GivenTo,CostOfOneDay,CostOfNDays,ServiceTaxRate,FinalSum) values('" + txtEquipmentName.Text + "','" + txtEquipmentGiven.Text + "','" + txtCostOneDay.Text + "','" + txtCostNDay.Text.ToString() + "','" + txtServiceTax.Text.ToString() + "','" + txtFinalSum.Text.ToString() + "')";
            SqlCommand cmd = new SqlCommand(str, con);
            cmd.ExecuteNonQuery();
            con.Close();
            Label1.Text = "Values Sucessfully Added to Database, Congratulations!";
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            int costOneDay = int.Parse(txtCostOneDay.Text); //10
            int costNday = int.Parse(txtCostNDay.Text); //5
            float serviceTax = float.Parse(txtServiceTax.Text) / 100; // 2/100 = 0.2
            

            float finalSum = (costOneDay * costNday) + (costOneDay * costNday * serviceTax); 

            txtFinalSum.Text = finalSum.ToString();

        }
    }
}