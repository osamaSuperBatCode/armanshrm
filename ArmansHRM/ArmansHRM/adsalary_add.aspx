﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adsalary_add.aspx.cs" Inherits="ArmansHRM.adsalary_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Salary Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            <asp:Label ID="lblEmpId" runat="server" Text="Employee Name: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlEmpId" CssClass="form-control m-bot15" runat="server" DataSourceID="SqlDataSource1" DataTextField="FirstName" DataValueField="FirstName">
                                
                            </asp:DropDownList>
                            </div><br /><br /><br />
                          <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [FirstName] FROM [personaldetails]"></asp:SqlDataSource>

                            <asp:Label ID="lblPaygradeName" runat="server" Text="Paygrade: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlPaygradeName" CssClass="form-control m-bot15" runat="server" DataSourceID="SqlDataSource2" DataTextField="NameOfPaygrade" DataValueField="NameOfPaygrade">
                                
                            </asp:DropDownList>
                            </div><br /><br /><br />
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" SelectCommand="SELECT [NameOfPaygrade] FROM [paygradedetails]"></asp:SqlDataSource>

                            <asp:RequiredFieldValidator ID="SalaryRequiredValidator" runat="server" ErrorMessage="Required Salary!" ControlToValidate="txtSalary" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="SalaryRegexValidator" ControlToValidate="txtSalary" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="lblSalary" runat="server" Text="Salary: " CssClass="control-label col-sm-2"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtSalary" runat="server" CssClass="form-control"></asp:TextBox>
                            </div><br /><br /><br />


                             <asp:Label ID="lblLeavesGreater" runat="server" Text="Leaves greater than 10?: " CssClass="control-label col-sm-2"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlLeavesDeduction" CssClass="form-control m-bot15" runat="server">
                                <asp:ListItem>Yes</asp:ListItem>                           
                                <asp:ListItem>No</asp:ListItem>
                            </asp:DropDownList>
                            </div><br /><br /><br />


                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required Salary!" ControlToValidate="txtSalary" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="TextBox1" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="Label1" runat="server" Text="Final Salary: " CssClass="control-label col-sm-2"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="TextBox1" onfocus="blur()" runat="server" CssClass="form-control"></asp:TextBox>
                            </div><br /><br /><br />

                            

                            <div class="col-sm-5"></div>
                            <asp:Button ID="Button2" runat="server" Text="Calculate" CssClass="btn btn-warning" OnClick="Button2_Click" />
                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-info" OnClick="Button1_Click" />
                            <asp:Label ID="Label2" runat="server" Text="" CssClass="help-block alert-success"></asp:Label>


                         </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
