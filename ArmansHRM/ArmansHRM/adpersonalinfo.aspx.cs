﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ArmansHRM
{
    public partial class adpersonalinfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["type"] != "Admin")
            {
                Response.Redirect("login.aspx");
            }

            hrmdbSqlDataSource.SelectCommand = "SELECT * FROM personaldetails WHERE Username = '"+Session["username"]+"'";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("adregisteration.aspx");
        }
    }
}