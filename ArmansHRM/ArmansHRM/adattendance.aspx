﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adattendance.aspx.cs" Inherits="ArmansHRM.adattendance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Attendance Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
    <form class="form-horizontal tasi-form" runat="server">


        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="SerialNumber" DataSourceID="SqlDataSource1" CssClass="table table-responsive table-bordered table-striped">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:TemplateField HeaderText="SerialNumber" InsertVisible="False" SortExpression="SerialNumber">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EmployeeName" SortExpression="EmployeeName">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" ReadOnly="true" Text='<%# Bind("EmployeeName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("EmployeeName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PunchInTime" SortExpression="PunchInTime">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="PunchInTimeRequiredValidator" runat="server" ErrorMessage="Required Time!" ControlToValidate="TextBox2" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("PunchInTime") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("PunchInTime") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PunchOutTime" SortExpression="PunchOutTime">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="reqval" runat="server" ErrorMessage="Required Time!" ControlToValidate="TextBox3" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("PunchOutTime") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("PunchOutTime") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date" SortExpression="Date">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="RequiredValidator" runat="server" ErrorMessage="Required Date!" ControlToValidate="TextBox4" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("Date") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="AdditionalNotes" SortExpression="AdditionalNotes">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("AdditionalNotes") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("AdditionalNotes") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>



        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [attendancedetails] WHERE [SerialNumber] = @original_SerialNumber AND (([EmployeeName] = @original_EmployeeName) OR ([EmployeeName] IS NULL AND @original_EmployeeName IS NULL)) AND (([PunchInTime] = @original_PunchInTime) OR ([PunchInTime] IS NULL AND @original_PunchInTime IS NULL)) AND (([PunchOutTime] = @original_PunchOutTime) OR ([PunchOutTime] IS NULL AND @original_PunchOutTime IS NULL)) AND (([Date] = @original_Date) OR ([Date] IS NULL AND @original_Date IS NULL)) AND (([AdditionalNotes] = @original_AdditionalNotes) OR ([AdditionalNotes] IS NULL AND @original_AdditionalNotes IS NULL))" InsertCommand="INSERT INTO [attendancedetails] ([EmployeeName], [PunchInTime], [PunchOutTime], [Date], [AdditionalNotes]) VALUES (@EmployeeName, @PunchInTime, @PunchOutTime, @Date, @AdditionalNotes)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [attendancedetails]" UpdateCommand="UPDATE [attendancedetails] SET [EmployeeName] = @EmployeeName, [PunchInTime] = @PunchInTime, [PunchOutTime] = @PunchOutTime, [Date] = @Date, [AdditionalNotes] = @AdditionalNotes WHERE [SerialNumber] = @original_SerialNumber AND (([EmployeeName] = @original_EmployeeName) OR ([EmployeeName] IS NULL AND @original_EmployeeName IS NULL)) AND (([PunchInTime] = @original_PunchInTime) OR ([PunchInTime] IS NULL AND @original_PunchInTime IS NULL)) AND (([PunchOutTime] = @original_PunchOutTime) OR ([PunchOutTime] IS NULL AND @original_PunchOutTime IS NULL)) AND (([Date] = @original_Date) OR ([Date] IS NULL AND @original_Date IS NULL)) AND (([AdditionalNotes] = @original_AdditionalNotes) OR ([AdditionalNotes] IS NULL AND @original_AdditionalNotes IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                <asp:Parameter Name="original_EmployeeName" Type="String" />
                <asp:Parameter Name="original_PunchInTime" Type="String" />
                <asp:Parameter Name="original_PunchOutTime" Type="String" />
                <asp:Parameter Name="original_Date" Type="String" />
                <asp:Parameter Name="original_AdditionalNotes" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="EmployeeName" Type="String" />
                <asp:Parameter Name="PunchInTime" Type="String" />
                <asp:Parameter Name="PunchOutTime" Type="String" />
                <asp:Parameter Name="Date" Type="String" />
                <asp:Parameter Name="AdditionalNotes" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="EmployeeName" Type="String" />
                <asp:Parameter Name="PunchInTime" Type="String" />
                <asp:Parameter Name="PunchOutTime" Type="String" />
                <asp:Parameter Name="Date" Type="String" />
                <asp:Parameter Name="AdditionalNotes" Type="String" />
                <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                <asp:Parameter Name="original_EmployeeName" Type="String" />
                <asp:Parameter Name="original_PunchInTime" Type="String" />
                <asp:Parameter Name="original_PunchOutTime" Type="String" />
                <asp:Parameter Name="original_Date" Type="String" />
                <asp:Parameter Name="original_AdditionalNotes" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>



    <br /><br /><br />
    <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Add New Value" CssClass="btn btn-danger" OnClick="Button1_Click" />
        </form>
          </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
