﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adequipments.aspx.cs" Inherits="ArmansHRM.adequipments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Equipments Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
    <form class="form-horizontal tasi-form" runat="server">

        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="SerialNumber" DataSourceID="SqlDataSource1" CssClass="table table-responsive table-bordered table-hover table-striped">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:TemplateField HeaderText="SerialNumber" InsertVisible="False" SortExpression="SerialNumber">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("SerialNumber") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="EquipmentName" SortExpression="EquipmentName">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="EquipmentNameRequiredValidator" runat="server" ErrorMessage="Required Equipment!" ControlToValidate="TextBox1" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("EquipmentName") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("EquipmentName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="GivenTo" SortExpression="GivenTo">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="EquipmentGivenRequiredValidator" runat="server" ErrorMessage="Equipment Required!" ControlToValidate="TextBox2" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("GivenTo") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("GivenTo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CostOfOneDay" SortExpression="CostOfOneDay">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="CostOneDayRequiredValidator" runat="server" ErrorMessage="Amount Required!" ControlToValidate="TextBox3" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="CostOneDaysRegexValidator" ControlToValidate="TextBox3" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("CostOfOneDay") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("CostOfOneDay") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CostOfNDays" SortExpression="CostOfNDays">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Amount Required!" ControlToValidate="TextBox4" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="textBox4" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("CostOfNDays") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("CostOfNDays") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ServiceTaxRate" SortExpression="ServiceTaxRate">
                    <EditItemTemplate>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Amount Required!" ControlToValidate="TextBox5" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ControlToValidate="TextBox5" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("ServiceTaxRate") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("ServiceTaxRate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FinalSum" SortExpression="FinalSum">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox6" onfocus="blur()" runat="server" Text='<%# Bind("FinalSum") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text='<%# Bind("FinalSum") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:hrmdbConnectionString %>" DeleteCommand="DELETE FROM [equipmentdetails] WHERE [SerialNumber] = @original_SerialNumber AND (([EquipmentName] = @original_EquipmentName) OR ([EquipmentName] IS NULL AND @original_EquipmentName IS NULL)) AND (([GivenTo] = @original_GivenTo) OR ([GivenTo] IS NULL AND @original_GivenTo IS NULL)) AND (([CostOfOneDay] = @original_CostOfOneDay) OR ([CostOfOneDay] IS NULL AND @original_CostOfOneDay IS NULL)) AND (([CostOfNDays] = @original_CostOfNDays) OR ([CostOfNDays] IS NULL AND @original_CostOfNDays IS NULL)) AND (([ServiceTaxRate] = @original_ServiceTaxRate) OR ([ServiceTaxRate] IS NULL AND @original_ServiceTaxRate IS NULL)) AND (([FinalSum] = @original_FinalSum) OR ([FinalSum] IS NULL AND @original_FinalSum IS NULL))" InsertCommand="INSERT INTO [equipmentdetails] ([EquipmentName], [GivenTo], [CostOfOneDay], [CostOfNDays], [ServiceTaxRate], [FinalSum]) VALUES (@EquipmentName, @GivenTo, @CostOfOneDay, @CostOfNDays, @ServiceTaxRate, @FinalSum)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [equipmentdetails]" UpdateCommand="UPDATE [equipmentdetails] SET [EquipmentName] = @EquipmentName, [GivenTo] = @GivenTo, [CostOfOneDay] = @CostOfOneDay, [CostOfNDays] = @CostOfNDays, [ServiceTaxRate] = @ServiceTaxRate, [FinalSum] = @FinalSum WHERE [SerialNumber] = @original_SerialNumber AND (([EquipmentName] = @original_EquipmentName) OR ([EquipmentName] IS NULL AND @original_EquipmentName IS NULL)) AND (([GivenTo] = @original_GivenTo) OR ([GivenTo] IS NULL AND @original_GivenTo IS NULL)) AND (([CostOfOneDay] = @original_CostOfOneDay) OR ([CostOfOneDay] IS NULL AND @original_CostOfOneDay IS NULL)) AND (([CostOfNDays] = @original_CostOfNDays) OR ([CostOfNDays] IS NULL AND @original_CostOfNDays IS NULL)) AND (([ServiceTaxRate] = @original_ServiceTaxRate) OR ([ServiceTaxRate] IS NULL AND @original_ServiceTaxRate IS NULL)) AND (([FinalSum] = @original_FinalSum) OR ([FinalSum] IS NULL AND @original_FinalSum IS NULL))">
            <DeleteParameters>
                <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                <asp:Parameter Name="original_EquipmentName" Type="String" />
                <asp:Parameter Name="original_GivenTo" Type="String" />
                <asp:Parameter Name="original_CostOfOneDay" Type="String" />
                <asp:Parameter Name="original_CostOfNDays" Type="String" />
                <asp:Parameter Name="original_ServiceTaxRate" Type="String" />
                <asp:Parameter Name="original_FinalSum" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="EquipmentName" Type="String" />
                <asp:Parameter Name="GivenTo" Type="String" />
                <asp:Parameter Name="CostOfOneDay" Type="String" />
                <asp:Parameter Name="CostOfNDays" Type="String" />
                <asp:Parameter Name="ServiceTaxRate" Type="String" />
                <asp:Parameter Name="FinalSum" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="EquipmentName" Type="String" />
                <asp:Parameter Name="GivenTo" Type="String" />
                <asp:Parameter Name="CostOfOneDay" Type="String" />
                <asp:Parameter Name="CostOfNDays" Type="String" />
                <asp:Parameter Name="ServiceTaxRate" Type="String" />
                <asp:Parameter Name="FinalSum" Type="String" />
                <asp:Parameter Name="original_SerialNumber" Type="Int32" />
                <asp:Parameter Name="original_EquipmentName" Type="String" />
                <asp:Parameter Name="original_GivenTo" Type="String" />
                <asp:Parameter Name="original_CostOfOneDay" Type="String" />
                <asp:Parameter Name="original_CostOfNDays" Type="String" />
                <asp:Parameter Name="original_ServiceTaxRate" Type="String" />
                <asp:Parameter Name="original_FinalSum" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>

        <br /><br /><br />
    <div class="col-sm-5"></div>
        <asp:Button ID="Button2" runat="server" Text="Calculate" CssClass="btn btn-warning" OnClick="Button2_Click" />
        <asp:Button ID="Button1" runat="server" Text="Add New Value" CssClass="btn btn-danger" OnClick="Button1_Click" />
        </form>
          </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
