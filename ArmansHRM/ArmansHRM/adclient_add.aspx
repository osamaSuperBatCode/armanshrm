﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adclient_add.aspx.cs" Inherits="ArmansHRM.adclient_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Client Information Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            
                            <asp:Label ID="lblClientName" runat="server" Text="Client Name: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtClientName" runat="server" CssClass="form-control" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ClientNameRequiredValidator" runat="server" ErrorMessage="Every person has a name..." ControlToValidate="txtClientName" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block  col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="ClientNameRegexValidator" ControlToValidate="txtClientName" runat="server" ErrorMessage="Numerical name? :O" ValidationExpression="[a-z]*" Display="Dynamic" CssClass="help-block  col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="ContactNumberRequiredValidator" runat="server" ErrorMessage="Required!" ControlToValidate="txtContactNumber" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block "></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="ContactNumberRegexValidator" ControlToValidate="txtContactNumber" runat="server" ErrorMessage="Only Numbers, hotshot :)" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block " ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="lblContactNumber" runat="server" Text="Contact Number: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtContactNumber" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />
                            
                            <asp:RequiredFieldValidator ID="ClientEmailRequiredValidator" runat="server" ErrorMessage="Emails are a good way to keep contact" ControlToValidate="txtClientEmail" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="ClientEmailRegexValidator" ControlToValidate="txtClientEmail" runat="server" ErrorMessage="0mgh4x0rs Invalid Email!!" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42" ValidateRequestMode="Inherit" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            <asp:Label ID="lblClientEmail" runat="server" Text="Client Email: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtClientEmail" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                            <asp:Label ID="lblDetails" runat="server" Text="Details: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtDetails" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </div><br /><br /><br /><br />

                            <asp:RequiredFieldValidator ID="ClientAddressRequiredValidator" runat="server" ErrorMessage="Required" ControlToValidate="txtClientAddress" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblClientAddress" runat="server" Text="Client Address: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtClientAddress" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </div><br /><br /><br /><br />

                            <asp:Label ID="lblStatus" runat="server" Text="Status: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:DropDownList ID="ddlStatus" CssClass="form-control m-bot15" runat="server">
                                <asp:ListItem>Active</asp:ListItem>
                                <asp:ListItem>Inactive</asp:ListItem>
                            </asp:DropDownList>
                            </div><br /><br /><br />

                            <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-info" OnClick="Button1_Click" /><br /><br /><br />
                            <div class="col-sm-5"></div>
                            <asp:Label ID="Label1" runat="server" Text="" CssClass="alert-success"></asp:Label>
                            

                         </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
