﻿<%@ Page Title="" Language="C#" MasterPageFile="~/adminmaster.Master" AutoEventWireup="true" CodeBehind="adpaygrade_add.aspx.cs" Inherits="ArmansHRM.adpaygrade_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <section id="main-content">
          <section class="wrapper">
              <div class="row">
                  <div class="col-lg-12">
                      <section class="panel">
                          <header class="panel-heading">
                              Paygrade Module
                          </header>
                          <div class="panel-body">
              <section class="panel">
                   <div class="form-group">
                        <form class="form-horizontal tasi-form" runat="server">

                            
                          
                            <asp:Label ID="lblPaygradeName" runat="server" Text="Paygrade Name: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtPaygradeName" runat="server" CssClass="form-control" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="PaygradeNameRequiredValidator" runat="server" ErrorMessage="Required Paygrade Name!" ControlToValidate="txtPaygradeName" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="MinSalaryRequiredValidator" runat="server" ErrorMessage="Minimum Salary Required!" ControlToValidate="txtMinimumSalary" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblMinimumSalary" runat="server" Text="Minimum Salary: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <asp:RegularExpressionValidator ID="MinimumSalaryRegexValidator" ControlToValidate="txtMinimumSalary" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtMinimumSalary" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                            <asp:RequiredFieldValidator ID="MaximumSalaryRequiredValidator" runat="server" ErrorMessage="Maximum Salary Required!" ControlToValidate="txtMaximumSalary" Display="Dynamic" ForeColor="#C66C42" CssClass="help-block col-sm-push-2"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="MaximumSalaryRegexValidator" ControlToValidate="txtMaximumSalary" runat="server" ErrorMessage="Only Numbers" ValidationExpression="\d+" Display="Dynamic" CssClass="help-block col-sm-push-2" ForeColor="#C66C42"></asp:RegularExpressionValidator>
                            <asp:Label ID="lblMaximumSalary" runat="server" Text="Maximum Salary: " CssClass="col-sm-2 col-sm-2 control-label"></asp:Label>
                            <div class="col-sm-8">
                            <asp:TextBox ID="txtMaximumSalary" runat="server" CssClass="form-control" ></asp:TextBox>
                            </div><br /><br /><br />

                            <div class="col-sm-5"></div>
                            <asp:Button ID="Button1" runat="server" Text="Submit" CssClass="btn btn-info" OnClick="Button1_Click" />
                            <asp:Label ID="Label1" runat="server" Text="" CssClass="alert-success"></asp:Label>

                         </form>
                   </div>
                </section>
                </div>
            </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
