USE [hrmdb]
GO
SET IDENTITY_INSERT [dbo].[adminlogin] ON 

INSERT [dbo].[adminlogin] ([SerialNumber], [username], [password]) VALUES (14, N'adminone', N'adminone')
INSERT [dbo].[adminlogin] ([SerialNumber], [username], [password]) VALUES (15, N'armanadmin', N'armanadmin')
INSERT [dbo].[adminlogin] ([SerialNumber], [username], [password]) VALUES (16, N'osamaadmin', N'osamaadmin')
SET IDENTITY_INSERT [dbo].[adminlogin] OFF
SET IDENTITY_INSERT [dbo].[clientdetails] ON 

INSERT [dbo].[clientdetails] ([SerialNumber], [Name], [ContactNumber], [ClientEmail], [Details], [Address], [Status]) VALUES (5, N'Piyush Verma', N'9867584756', N'piyush.pro99@gmail.com', N'Small Contracts', N'Woodpark Yari Road', N'Active')
INSERT [dbo].[clientdetails] ([SerialNumber], [Name], [ContactNumber], [ClientEmail], [Details], [Address], [Status]) VALUES (6, N'Payal Chaudhry', N'8108574657', N'me.payals47@gmail.com', N'Ocassional Contracts', N'Sea View Building, 7 Bunglows', N'Active')
SET IDENTITY_INSERT [dbo].[clientdetails] OFF
SET IDENTITY_INSERT [dbo].[employeedetails] ON 

INSERT [dbo].[employeedetails] ([EmpID], [Username], [Type]) VALUES (3, N'adminone', N'Admin')
SET IDENTITY_INSERT [dbo].[employeedetails] OFF
SET IDENTITY_INSERT [dbo].[employeelogin] ON 

INSERT [dbo].[employeelogin] ([SerialNumber], [username], [password]) VALUES (2, N'saifakhtaremp', N'saifakhtaremp')
INSERT [dbo].[employeelogin] ([SerialNumber], [username], [password]) VALUES (3, N'rubenfernemp', N'rubenfernemp')
INSERT [dbo].[employeelogin] ([SerialNumber], [username], [password]) VALUES (4, N'rajeshdilemp', N'rajeshdilemp')
SET IDENTITY_INSERT [dbo].[employeelogin] OFF
SET IDENTITY_INSERT [dbo].[equipmentdetails] ON 

INSERT [dbo].[equipmentdetails] ([SerialNumber], [EquipmentName], [GivenTo], [CostOfOneDay], [CostOfNDays], [ServiceTaxRate], [FinalSum]) VALUES (5, N'Canon 550D', N'Ravi Jhetkumar', N'500', N'7', N'2', N'3570')
INSERT [dbo].[equipmentdetails] ([SerialNumber], [EquipmentName], [GivenTo], [CostOfOneDay], [CostOfNDays], [ServiceTaxRate], [FinalSum]) VALUES (6, N'Studio Lights', N'Ravi Jhetkumar', N'1500', N'3', N'2', N'4590')
SET IDENTITY_INSERT [dbo].[equipmentdetails] OFF
SET IDENTITY_INSERT [dbo].[expensedetails] ON 

INSERT [dbo].[expensedetails] ([SerialNumber], [EmployeeName], [InitialSum], [FinalSum], [TravelExpense], [TotalCost]) VALUES (3, N'Arman Sandhu', N'5350', N'4500', N'250', N'5100')
SET IDENTITY_INSERT [dbo].[expensedetails] OFF
SET IDENTITY_INSERT [dbo].[leavesdetails] ON 

INSERT [dbo].[leavesdetails] ([SerialNumber], [EmployeeName], [LeaveTypeName], [StartDate], [EndDate]) VALUES (5, N'Arman Sandhu', N'Moms Birthday Leave', N'13 May 2014 - 01:40', N'15 May 2014 - 14:40')
SET IDENTITY_INSERT [dbo].[leavesdetails] OFF
SET IDENTITY_INSERT [dbo].[paygradedetails] ON 

INSERT [dbo].[paygradedetails] ([SerialNumber], [NameOfPaygrade], [MinSalary], [MaxSalary]) VALUES (3, N'Phtography Level One', N'5000', N'10000')
INSERT [dbo].[paygradedetails] ([SerialNumber], [NameOfPaygrade], [MinSalary], [MaxSalary]) VALUES (4, N'Photography Level Two', N'12000', N'20000')
INSERT [dbo].[paygradedetails] ([SerialNumber], [NameOfPaygrade], [MinSalary], [MaxSalary]) VALUES (5, N'Photoshop Level One', N'7500', N'12000')
INSERT [dbo].[paygradedetails] ([SerialNumber], [NameOfPaygrade], [MinSalary], [MaxSalary]) VALUES (6, N'Photoshop Level Two', N'10000', N'18000')
SET IDENTITY_INSERT [dbo].[paygradedetails] OFF
SET IDENTITY_INSERT [dbo].[personaldetails] ON 

INSERT [dbo].[personaldetails] ([SerialNumber], [Username], [Type], [FirstName], [DateOfBirth], [Gender], [Department], [EmployeeStatus], [Paygrade], [Supervisor], [Qualification]) VALUES (15, N'armanadmin', N'Admin', N'Arman Sandhu', N'17 October 1985 - 02:10', N'Male', N'Photography', N'Full Time', N'Photography Level Two', N'', N'MAAC Trained Photographer')
INSERT [dbo].[personaldetails] ([SerialNumber], [Username], [Type], [FirstName], [DateOfBirth], [Gender], [Department], [EmployeeStatus], [Paygrade], [Supervisor], [Qualification]) VALUES (16, N'osamaadmin', N'Admin', N'Osama Iqbal', N'24 September 1986 - 12:15', N'Male', N'Photoshop', N'Full Time', N'Photoshop Level Two', N'Arman Sandhu', N'Kebly Photoshop Training')
INSERT [dbo].[personaldetails] ([SerialNumber], [Username], [Type], [FirstName], [DateOfBirth], [Gender], [Department], [EmployeeStatus], [Paygrade], [Supervisor], [Qualification]) VALUES (17, N'saifakhtaremp', N'Employee', N'Saif Akhtar', N'13 February 1990 - 10:50', N'Male', N'Photoshop', N'Full Time', N'Photoshop Level One', N'Osama Iqbal', N'MAAC Photoshop Basics')
INSERT [dbo].[personaldetails] ([SerialNumber], [Username], [Type], [FirstName], [DateOfBirth], [Gender], [Department], [EmployeeStatus], [Paygrade], [Supervisor], [Qualification]) VALUES (18, N'rubenfernemp', N'Employee', N'Ruben Fernandes', N'28 February 1991 - 06:30', N'Male', N'Photography', N'Full Time', N'Photography Level Two', N'Arman Sandhu', N'MAAC Trained Photographer')
INSERT [dbo].[personaldetails] ([SerialNumber], [Username], [Type], [FirstName], [DateOfBirth], [Gender], [Department], [EmployeeStatus], [Paygrade], [Supervisor], [Qualification]) VALUES (19, N'rajeshdilemp', N'Employee', N'Rajesh Dilwaar', N'02 July 1993 - 01:20', N'Male', N'Photography', N'Part Time', N'Phtography Level One', N'Arman Sandhu', N'Trainee')
SET IDENTITY_INSERT [dbo].[personaldetails] OFF
SET IDENTITY_INSERT [dbo].[projectdetails] ON 

INSERT [dbo].[projectdetails] ([SerialNumber], [EmployeeName], [NameOfProject], [ClientName], [Details], [Status]) VALUES (3, N'Arman Sandhu', N'wedding', N'Payal Chaudhry', N'Wedding at 4 Bunglows Church', N'Active')
SET IDENTITY_INSERT [dbo].[projectdetails] OFF
SET IDENTITY_INSERT [dbo].[salarydetails] ON 

INSERT [dbo].[salarydetails] ([SerialNumber], [EmployeeName], [PaygradeName], [Salary], [LeavesReduction], [FinalSalary]) VALUES (3, N'Arman Sandhu', N'Phtography Level Two', N'18000', N'No', N'18000')
INSERT [dbo].[salarydetails] ([SerialNumber], [EmployeeName], [PaygradeName], [Salary], [LeavesReduction], [FinalSalary]) VALUES (4, N'Osama Iqbal', N'Photoshop Level Two', N'17000', N'No', N'17000')
INSERT [dbo].[salarydetails] ([SerialNumber], [EmployeeName], [PaygradeName], [Salary], [LeavesReduction], [FinalSalary]) VALUES (5, N'Saif Akhtar', N'Photoshop Level One', N'6000', N'No', N'6000')
INSERT [dbo].[salarydetails] ([SerialNumber], [EmployeeName], [PaygradeName], [Salary], [LeavesReduction], [FinalSalary]) VALUES (6, N'Ruben Fernandes', N'Photography Level Two', N'12000', N'No', N'12000')
INSERT [dbo].[salarydetails] ([SerialNumber], [EmployeeName], [PaygradeName], [Salary], [LeavesReduction], [FinalSalary]) VALUES (7, N'Rajesh Dilwaar', N'Phtography Level One', N'5000', N'No', N'5000')
SET IDENTITY_INSERT [dbo].[salarydetails] OFF
SET IDENTITY_INSERT [dbo].[workshiftdetails] ON 

INSERT [dbo].[workshiftdetails] ([SerialNumber], [EmployeeName], [Type], [Duration]) VALUES (5, N'Arman Sandhu', N'Morning', N'8')
INSERT [dbo].[workshiftdetails] ([SerialNumber], [EmployeeName], [Type], [Duration]) VALUES (6, N'Osama Iqbal', N'Morning', N'10')
INSERT [dbo].[workshiftdetails] ([SerialNumber], [EmployeeName], [Type], [Duration]) VALUES (7, N'Saif Akhtar', N'Evening', N'6')
INSERT [dbo].[workshiftdetails] ([SerialNumber], [EmployeeName], [Type], [Duration]) VALUES (8, N'Ruben Fernandes', N'Evening', N'8')
INSERT [dbo].[workshiftdetails] ([SerialNumber], [EmployeeName], [Type], [Duration]) VALUES (9, N'Rajesh Dilwaar', N'Evening', N'9')
SET IDENTITY_INSERT [dbo].[workshiftdetails] OFF
